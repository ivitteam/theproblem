﻿using System.ComponentModel;
using System.Numerics;

namespace TheProblem.Logic.DTO
{
	public class NoteAmounts
	{
		[DefaultValue(0)]
		public int NumberOf10S { get; set; }
		[DefaultValue(0)]
		public int NumberOf20S { get; set; }
		[DefaultValue(0)]
		public int NumberOf50S { get; set; }
		[DefaultValue(0)]
		public BigInteger NumberOf100S { get; set; }
	}
}
