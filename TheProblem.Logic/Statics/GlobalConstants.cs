﻿namespace TheProblem.Logic.Statics
{
	public static class GlobalConstants
	{
		/// <summary>
		/// Exactly two digits after dot and 1 or more before
		/// </summary>
		public const string EntryFormat = @"^\d+.\d{2}$";
	}
}
