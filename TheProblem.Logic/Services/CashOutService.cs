﻿using System;
using System.Numerics;
using System.Text.RegularExpressions;
using TheProblem.Logic.DTO;
using TheProblem.Logic.Enums;
using TheProblem.Logic.Interfaces;
using TheProblem.Logic.Statics;

namespace TheProblem.Logic.Services
{
	public class CashOutService : ICashOutService
	{
		/// <summary>
		/// Validates if entry data can be solved as collection of notes
		/// </summary>
		/// <param name="entryData">String representing entry data</param>
		/// <returns></returns>
		public ResultType ValidateEntryAmount(string entryData)
		{
			if (string.IsNullOrEmpty(entryData)) return ResultType.Null;

			if (!Regex.IsMatch(entryData, GlobalConstants.EntryFormat)) return ResultType.InvalidArgument;

			if (!entryData.EndsWith("0.00")) return ResultType.NotCountable;

			if (entryData.Length < 6) return ResultType.Ok;

			return BigInteger.TryParse(entryData.Remove(entryData.Length - 5), out _)
				? ResultType.Ok
				: ResultType.OutOfMemoryException;
		}

		public NoteAmounts CountNotes(string entryData)
		{
			entryData = entryData.Remove(entryData.Length - 4);
			var result = CountSmallestNotes(entryData.AsSpan(entryData.Length-1));
			entryData = entryData.Remove(entryData.Length - 1);

			if (!BigInteger.TryParse(entryData, out var hundreadsCounter)) return result;

			result.NumberOf100S = hundreadsCounter;
			return result;
		}

		private static NoteAmounts CountSmallestNotes(ReadOnlySpan<char> lastChar)
		{
			var result = new NoteAmounts();
			var lastDigit = int.Parse(lastChar);

			if (lastDigit >= 5)
			{
				result.NumberOf50S = 1;
				lastDigit -= 5;
			}

			if (lastDigit >= 4)
			{
				result.NumberOf20S = 2;
				lastDigit -= 4;
			}

			if (lastDigit >= 2)
			{
				result.NumberOf20S = 1;
				lastDigit -= 2;
			}

			if (lastDigit >= 1)
			{
				result.NumberOf10S = 1;
			}

			return result;
		}
	}
}