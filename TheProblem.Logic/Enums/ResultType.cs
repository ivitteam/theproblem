﻿namespace TheProblem.Logic.Enums
{
	public enum ResultType
	{
		Ok,
		InvalidArgument,
		NotCountable,
		Null,
		OutOfMemoryException
	}
}
