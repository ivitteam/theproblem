﻿using TheProblem.Logic.DTO;
using TheProblem.Logic.Enums;

namespace TheProblem.Logic.Interfaces
{
	public interface ICashOutService
	{
		/// <summary>
		/// Validates if entry data can be solved as collection of notes
		/// </summary>
		/// <param name="entryData">String representing entry data</param>
		/// <returns></returns>
		ResultType ValidateEntryAmount(string entryData);

		NoteAmounts CountNotes(string entryData);
	}
}