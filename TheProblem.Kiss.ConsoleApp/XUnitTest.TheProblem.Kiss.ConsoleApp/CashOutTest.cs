using TheProblem.Kiss.ConsoleApp.Enums;
using TheProblem.Kiss.ConsoleApp.Statics;
using Xunit;

namespace XUnitTest.TheProblem.Kiss.ConsoleApp
{
	public class CashOutTest
	{
		[Fact]
		public void ClasifyEntryData_Valid1()
		{
			// Arrange  
			const string entryData = "30.00";
			const ResultType expectedValue = ResultType.Ok;
  
			// Act  
			var result = CashOut.ClasifyEntryData(entryData);
  
			//Assert 
			Assert.Equal(expectedValue, result); 
		}

		[Fact]
		public void ClasifyEntryData_Valid2()
		{
			// Arrange  
			const string entryData = "80.00";
			const ResultType expectedValue = ResultType.Ok;
  
			// Act  
			var result = CashOut.ClasifyEntryData(entryData);
  
			//Assert 
			Assert.Equal(expectedValue, result); 
		}

		[Fact]
		public void ClasifyEntryData_NotCountable()
		{
			// Arrange  
			const string entryData = "125.00";
			const ResultType expectedValue = ResultType.NotCountable;
  
			// Act  
			var result = CashOut.ClasifyEntryData(entryData);
  
			//Assert 
			Assert.Equal(expectedValue, result); 
		}

		[Fact]
		public void ClasifyEntryData_InvalidArgument()
		{
			// Arrange  
			const string entryData = "-130.00";
			const ResultType expectedValue = ResultType.InvalidArgument;
  
			// Act  
			var result = CashOut.ClasifyEntryData(entryData);
  
			//Assert 
			Assert.Equal(expectedValue, result); 
		}

		[Fact]
		public void ClasifyEntryData_Null()
		{
			// Arrange  
			const string entryData = null;
			const ResultType expectedValue = ResultType.Null;
  
			// Act  
			var result = CashOut.ClasifyEntryData(entryData);
  
			//Assert 
			Assert.Equal(expectedValue, result); 
		}
	}
}
