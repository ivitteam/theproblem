﻿using System;
using System.Numerics;
using System.Text.RegularExpressions;
using TheProblem.Kiss.ConsoleApp.Enums;

namespace TheProblem.Kiss.ConsoleApp.Statics
{
	public static class CashOut
	{
		private const string EntryFormat = @"^\d+.\d{2}$";

		public static ResultType ClasifyEntryData(string entryData)
		{
			if (string.IsNullOrEmpty(entryData)) return ResultType.Null;

			if (!Regex.IsMatch(entryData, CashOut.EntryFormat)) return ResultType.InvalidArgument;

			if (!entryData.EndsWith("0.00")) return ResultType.NotCountable;

			if (entryData.Length < 6) return ResultType.Ok;

			return BigInteger.TryParse(entryData.Remove(entryData.Length - 5), out _)
				? ResultType.Ok
				: ResultType.InvalidArgument;
		}

		public static void PrintNotes(string entryData)
		{
			entryData = entryData.Remove(entryData.Length - 4);
			var lastDigit = int.Parse(entryData.AsSpan(entryData.Length-1));
			
			Console.Write("[");
			var commaFlag = false;

			if (lastDigit >= 5)
			{
				Console.Write("50.00");
				commaFlag = true;
				lastDigit -= 5;
			}

			if (lastDigit >= 4)
			{
				if (commaFlag) Console.Write(",");
				Console.Write("20.00, 20.00");
				commaFlag = true;
				lastDigit -= 4;
			}

			if (lastDigit >= 2)
			{
				if (commaFlag) Console.Write(",");
				Console.Write("20.00");
				commaFlag = true;
				lastDigit -= 2;
			}

			if (lastDigit >= 1)
			{
				if (commaFlag) Console.Write(",");
				Console.Write("10.00");
				commaFlag = true;
			}

			entryData = entryData.Remove(entryData.Length - 1);
			if (!BigInteger.TryParse(entryData, out var hundreadsCounter))
			{
				Console.Write("]");
				return;
			}

			if (hundreadsCounter > 0)
			{
				if (commaFlag) Console.Write(",");
				for (var i = 1; i < hundreadsCounter - 1; i++)
				{
					Console.Write("100.00,");
				}
				Console.Write("100.00]");
			}
			else
			{
				Console.Write("]");
			}
			
			
		}
		
	}
}
