﻿using System;
using TheProblem.Kiss.ConsoleApp.CustomExceptions;
using TheProblem.Kiss.ConsoleApp.Enums;
using TheProblem.Kiss.ConsoleApp.Statics;

namespace TheProblem.Kiss.ConsoleApp
{
	internal static class Program
	{
		private static void Main()
		{
			Console.WriteLine("Enter dot separeted number, two digits after dot are needed.");
			var entryData = Console.ReadLine();
			Console.WriteLine("");

			switch (CashOut.ClasifyEntryData(entryData))
			{
				case ResultType.Ok:
					CashOut.PrintNotes(entryData);
					break;
				case ResultType.InvalidArgument: throw new InvalidArgumentException(entryData);
				case ResultType.NotCountable: throw new NoteUnavailableException(entryData);
				case ResultType.Null: 
					Console.WriteLine("[]");
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			Console.ReadKey();
		}
	}
}
