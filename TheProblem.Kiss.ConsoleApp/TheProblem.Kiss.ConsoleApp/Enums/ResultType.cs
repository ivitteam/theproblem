﻿namespace TheProblem.Kiss.ConsoleApp.Enums
{
	public enum ResultType
	{
		Ok,
		InvalidArgument,
		NotCountable,
		Null
	}
}
