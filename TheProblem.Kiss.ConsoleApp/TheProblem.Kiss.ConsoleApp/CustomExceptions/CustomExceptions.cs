﻿using System;

namespace TheProblem.Kiss.ConsoleApp.CustomExceptions
{
	public class InvalidArgumentException : Exception
	{
		public InvalidArgumentException(){}

		public InvalidArgumentException(string argument): base($"You gave us wrong argument: {argument}")
		{

		}
	}

	public class NoteUnavailableException : Exception
	{
		public NoteUnavailableException(){}

		public NoteUnavailableException(string amount): base($"Can't change this amount: {amount} to avaliable notes.")
		{

		}
	}
}
