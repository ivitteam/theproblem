TheProblem

CONCEPT:

Develop a solution that simulate the delivery of notes when a client does a withdraw in a cash machine.

The basic requirements are the follow:
- Always deliver the lowest number of possible notes;
- It’s possible to get the amount requested with available notes;
- The client balance is infinite;
- Amount of notes is infinite;
- Available notes $ 100,00; $ 50,00; $ 20,00; $ 10,00


SOLUTIONS:

1. TheProblem.API - remote address https://api.ivit.org.pl/swagger/index.html
	- TheProblem.API - .NET CORE WebAPI
	- TheProblem.Logic - business logic class library for API
	- XUnitTest.TheProblem.API - simple xUnit tests for API
	
2. TheProblem.Web - remote address https://theproblem.ivit.org.pl/
	- TheProblem.Web - RazorPages front-end application
	
3. TheProblem.Kiss.ConsoleApp - Keep it stupid simple version
	- TheProblem.Kiss.ConsoleApp - .NET CORE Console App
	- XUnitTest.TheProblem.Kiss.ConsoleApp - simple xUnit tests for console app

	
INSTRUCTIONS:

1. API + Web (remote) - test it on https://theproblem.ivit.org.pl/ or https://api.ivit.org.pl/swagger/index.html.
2. API + Web (local) - run both project on local IIS, update API url in Web project
3. Console App - run it using VS2017 or in cmd "dotnet TheProblem.Kiss.ConsoleApp.dll"

