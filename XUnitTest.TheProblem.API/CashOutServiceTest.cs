using TheProblem.Logic.DTO;
using TheProblem.Logic.Enums;
using TheProblem.Logic.Interfaces;
using TheProblem.Logic.Services;
using Xunit;

namespace XUnitTest.TheProblem.API
{
	public class CashOutServiceTest
	{
		private readonly ICashOutService _cashOutService;

		public CashOutServiceTest()
		{
			_cashOutService = new CashOutService();
		}

		[Fact]
		public void CheckNoteAmounts_10()
		{
			// Arrange  
			const string entryData = "10.00";
			var expectedValue = new NoteAmounts
			{
				NumberOf10S = 1, 
				NumberOf20S = 0, 
				NumberOf50S = 0, 
				NumberOf100S = 0
			};

			// Act  
			var result = _cashOutService.CountNotes(entryData);

			//Assert 
			Assert.Equal(expectedValue.NumberOf10S, result.NumberOf10S);
			Assert.Equal(expectedValue.NumberOf20S, result.NumberOf20S);
			Assert.Equal(expectedValue.NumberOf50S, result.NumberOf50S);
			Assert.Equal(expectedValue.NumberOf100S, result.NumberOf100S);
		}

		[Fact]
		public void CheckNoteAmounts_30()
		{
			// Arrange  
			const string entryData = "30.00";
			var expectedValue = new NoteAmounts
			{
				NumberOf10S = 1, 
				NumberOf20S = 1, 
				NumberOf50S = 0, 
				NumberOf100S = 0
			};

			// Act  
			var result = _cashOutService.CountNotes(entryData);

			//Assert 
			Assert.Equal(expectedValue.NumberOf10S, result.NumberOf10S);
			Assert.Equal(expectedValue.NumberOf20S, result.NumberOf20S);
			Assert.Equal(expectedValue.NumberOf50S, result.NumberOf50S);
			Assert.Equal(expectedValue.NumberOf100S, result.NumberOf100S);
		}
		
		[Fact]
		public void CheckNoteAmounts_90()
		{
			// Arrange  
			const string entryData = "90.00";
			var expectedValue = new NoteAmounts
			{
				NumberOf10S = 0, 
				NumberOf20S = 2, 
				NumberOf50S = 1, 
				NumberOf100S = 0
			};

			// Act  
			var result = _cashOutService.CountNotes(entryData);

			//Assert 
			Assert.Equal(expectedValue.NumberOf10S, result.NumberOf10S);
			Assert.Equal(expectedValue.NumberOf20S, result.NumberOf20S);
			Assert.Equal(expectedValue.NumberOf50S, result.NumberOf50S);
			Assert.Equal(expectedValue.NumberOf100S, result.NumberOf100S);
		}
		[Fact]
		public void CheckNoteAmounts_130()
		{
			// Arrange  
			const string entryData = "130.00";
			var expectedValue = new NoteAmounts
			{
				NumberOf10S = 1, 
				NumberOf20S = 1, 
				NumberOf50S = 0, 
				NumberOf100S = 1
			};

			// Act  
			var result = _cashOutService.CountNotes(entryData);

			//Assert 
			Assert.Equal(expectedValue.NumberOf10S, result.NumberOf10S);
			Assert.Equal(expectedValue.NumberOf20S, result.NumberOf20S);
			Assert.Equal(expectedValue.NumberOf50S, result.NumberOf50S);
			Assert.Equal(expectedValue.NumberOf100S, result.NumberOf100S);
		}
		[Fact]
		public void CheckNoteAmounts_400()
		{
			// Arrange  
			const string entryData = "400.00";
			var expectedValue = new NoteAmounts
			{
				NumberOf10S = 0, 
				NumberOf20S = 0, 
				NumberOf50S = 0, 
				NumberOf100S = 4
			};

			// Act  
			var result = _cashOutService.CountNotes(entryData);

			//Assert 
			Assert.Equal(expectedValue.NumberOf10S, result.NumberOf10S);
			Assert.Equal(expectedValue.NumberOf20S, result.NumberOf20S);
			Assert.Equal(expectedValue.NumberOf50S, result.NumberOf50S);
			Assert.Equal(expectedValue.NumberOf100S, result.NumberOf100S);
		}
		[Fact]
		public void CheckNoteAmounts_634654756756756756730()
		{
			// Arrange  
			const string entryData = "634654756756756756730.00";
			var expectedValue = new NoteAmounts
			{
				NumberOf10S = 1, 
				NumberOf20S = 1, 
				NumberOf50S = 0, 
				NumberOf100S = 6346547567567567567
			};

			// Act  
			var result = _cashOutService.CountNotes(entryData);

			//Assert 
			Assert.Equal(expectedValue.NumberOf10S, result.NumberOf10S);
			Assert.Equal(expectedValue.NumberOf20S, result.NumberOf20S);
			Assert.Equal(expectedValue.NumberOf50S, result.NumberOf50S);
			Assert.Equal(expectedValue.NumberOf100S, result.NumberOf100S);
		}

		[Fact]
		public void ClasifyEntryData_Valid1()
		{
			// Arrange  
			const string entryData = "30.00";
			const ResultType expectedValue = ResultType.Ok;

			// Act  
			var result = _cashOutService.ValidateEntryAmount(entryData);

			//Assert 
			Assert.Equal(expectedValue, result);
		}

		[Fact]
		public void ClasifyEntryData_Valid2()
		{
			// Arrange  
			const string entryData = "80.00";
			const ResultType expectedValue = ResultType.Ok;

			// Act  
			var result = _cashOutService.ValidateEntryAmount(entryData);

			//Assert 
			Assert.Equal(expectedValue, result);
		}

		[Fact]
		public void ClasifyEntryData_NotCountable()
		{
			// Arrange  
			const string entryData = "125.00";
			const ResultType expectedValue = ResultType.NotCountable;

			// Act  
			var result = _cashOutService.ValidateEntryAmount(entryData);

			//Assert 
			Assert.Equal(expectedValue, result);
		}

		[Fact]
		public void ClasifyEntryData_InvalidArgument()
		{
			// Arrange  
			const string entryData = "-130.00";
			const ResultType expectedValue = ResultType.InvalidArgument;

			// Act  
			var result = _cashOutService.ValidateEntryAmount(entryData);

			//Assert 
			Assert.Equal(expectedValue, result);
		}

		[Fact]
		public void ClasifyEntryData_Null()
		{
			// Arrange  
			const string entryData = null;
			const ResultType expectedValue = ResultType.Null;

			// Act  
			var result = _cashOutService.ValidateEntryAmount(entryData);

			//Assert 
			Assert.Equal(expectedValue, result);
		}
	}
}