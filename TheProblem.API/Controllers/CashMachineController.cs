﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using TheProblem.Logic.DTO;
using TheProblem.Logic.Enums;
using TheProblem.Logic.Interfaces;

namespace TheProblem.API.Controllers
{
	/// <summary>
	/// Main API controller used to solve The Problem
	/// </summary>
	[Route("api/[controller]")]
	[ApiController]
	public class CashMachineController : ControllerBase
	{
		private readonly ICashOutService _cashOutService;

		/// <inheritdoc />
		public CashMachineController(ICashOutService cashOutService)
		{
			_cashOutService = cashOutService;
		}

		/// <summary>
		/// Withdraw passed amount of $ from cashmachine
		/// </summary>
		/// <param name="amount">Amount of $</param>
		/// <returns>Amount of each notes type</returns>
		[HttpGet]
		[SwaggerOperation(Summary = "Gets amount of notes collection",
			Description = "Counts and returns amount in smallest number of notes or throws error")]
		[SwaggerResponse(200, "Ok")]
		[SwaggerResponse(204, "No Content")]
		[SwaggerResponse(400, "Invalid argument")]
		[SwaggerResponse(418, "I'm a teapot (note unavaliable")]
		public ActionResult<NoteAmounts> Withdraw(string amount)
		{
			switch (_cashOutService.ValidateEntryAmount(amount))
			{
				case ResultType.Ok: return Ok(_cashOutService.CountNotes(amount));
				case ResultType.InvalidArgument: return BadRequest("Invalid argument");
				case ResultType.NotCountable: return StatusCode(418, "It's not countable");
				case ResultType.Null: return NoContent();
				case ResultType.OutOfMemoryException: return BadRequest("Argument to large");
				default: return NotFound("Result not found");
			}
		}
	}
}