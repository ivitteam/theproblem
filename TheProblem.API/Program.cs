﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace TheProblem.API
{
    /// <summary>
    /// Auto-Generated code
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Auto-Generated code
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Auto-Generated code
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
